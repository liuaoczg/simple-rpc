package com.rpc.learn.serialize;

public class SerializerFactory {
    public static Serializer getSerializer() {
        return KryoSerializer.getInstance();
    }
}
