package com.rpc.learn.regist;

import java.net.InetSocketAddress;

/**
 * 注册中心
 */
public interface ServiceRegister {
    void regist(String serviceName);

    InetSocketAddress lookup(String serviceName);
}
