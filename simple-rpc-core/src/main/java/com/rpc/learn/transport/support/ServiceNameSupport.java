package com.rpc.learn.transport.support;

import com.rpc.learn.transport.exception.RemoteException;

public class ServiceNameSupport {
    //完整服务名格式    xxxx.实例名.方法名
    public static String[] split(String serviceName) {
        String[] split = serviceName.split("\\.");
        if(split.length < 2) {
            throw new RemoteException("服务名称不规范，至少符合 实例类名.方法 格式");
        }
        return split;
    }

    public static String getInstantName(String serviceName) {
        String[] split = split(serviceName);
        return split[split.length - 2];
    }
}
