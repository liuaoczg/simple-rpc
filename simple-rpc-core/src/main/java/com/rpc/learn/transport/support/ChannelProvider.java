package com.rpc.learn.transport.support;

import io.netty.channel.Channel;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ChannelProvider {
    private final Map<String, Channel> channelMap = new ConcurrentHashMap<>();

    public ChannelProvider() {
    }

    public Channel get(InetSocketAddress inetSocketAddress) {
        String key = inetSocketAddress.toString();
        Channel channel = channelMap.get(key);
        if(channel != null) {
            if(channel.isActive()) {
                return channel;
            }
        }
        return null;
    }

    public void put(InetSocketAddress address, Channel channel) {
        channelMap.put(address.toString(), channel);
    }
}
