package com.rpc.learn.provider;

import com.rpc.learn.regist.ServiceRegister;
import com.rpc.learn.regist.ZkServiceRegister;

import java.util.concurrent.ConcurrentHashMap;

/**
 * TODO 实际上应该是单例
 */
public class ServiceProvider {

    private static final ConcurrentHashMap<String, RemoteService> servicesMap = new ConcurrentHashMap<String, RemoteService>();
    private static final ServiceRegister serviceRegister;
    static {
        //可在配置文件中配置使用哪种注册中心
        serviceRegister = new ZkServiceRegister();
    }

    //发布服务的方法
    public static void publishService(String serviceName, RemoteService remoteService) {
        servicesMap.put(serviceName, remoteService);
        serviceRegister.regist(serviceName);
    }

    //查找服务的方法
    public static RemoteService find(String serviceName) {
        return servicesMap.get(serviceName);
    }
}
