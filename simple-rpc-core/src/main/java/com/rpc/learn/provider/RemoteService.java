package com.rpc.learn.provider;

/**
 * mark interface
 * 标记这是一个远程服务，无其他左右，后续结合spring 自动注入可以用注解替代
 */
public interface RemoteService {

}
