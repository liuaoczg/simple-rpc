package com.rpc.learn.loadbalance;

import java.util.List;
import java.util.Random;

public class RandomServiceLoadBalance implements ServiceLoadBalance{
    private Random random = new Random();
    @Override
    public String selectService(List<String> addresses) {
        if(addresses == null || addresses.size() == 0) {
            return null;
        }
        if(addresses.size() == 1) {
            return addresses.get(0);
        }
        return addresses.get(random.nextInt(addresses.size()));
    }
}
