package com.rpc.learn.loadbalance;

import java.util.List;

public interface ServiceLoadBalance {
    String selectService(List<String> addresses);
}
