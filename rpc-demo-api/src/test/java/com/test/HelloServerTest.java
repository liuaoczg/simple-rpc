package com.test;

import com.rpc.learc.HelloService;
import com.rpc.learn.provider.ServiceProvider;
import com.rpc.learn.transport.NettyRpcServer;

public class HelloServerTest {
    public static void main(String[] args) throws Exception {
        ServiceProvider.publishService("HelloService", new HelloService());

        NettyRpcServer server = new NettyRpcServer();
        server.start();
    }
}
