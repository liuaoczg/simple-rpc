package com.test;

import com.rpc.learn.consume.ServiceConsumer;

public class ClientTest {
    public static void main(String[] args) {
        Object hello = ServiceConsumer.invoke("HelloService.hello", "miemie");
        System.out.println(hello);
    }
}
