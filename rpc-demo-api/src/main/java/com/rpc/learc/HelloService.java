package com.rpc.learc;


import com.rpc.learn.provider.RemoteService;

public class HelloService implements RemoteService{

    public String hello(String word) {
        System.out.println("client say:" + word);
        return "hello client";
    }
//    @Override
//    public String invoke(Object ... params) {
//        for (Object param : params) {
//            System.out.println(param);
//        }
//        return "hello client";
//    }
}
